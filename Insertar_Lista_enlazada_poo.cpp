#include <iostream>

using namespace std;


class Nodo{

public:
    int dato;
    Nodo *siguiente;

    Nodo(){
        this->dato=0;
        this->siguiente=NULL;
    }

};

class Pila{

public:
    Nodo *tope;
    int tamanio;

    Pila(){
        this->tamanio=0;
        this->tope=NULL;
    }

    void Insertar(int dato){

        Nodo *nuevo_nodo = new Nodo();
        nuevo_nodo->dato = dato;
        nuevo_nodo->siguiente = this->tope;
        this->tope=nuevo_nodo;
        cout<<"\nNodo agregado"<<endl;
        this->tamanio++;
    }
};



int main(){

    int dato, opcion;
    Pila pila = Pila();

    do{

        cout<<"\n[1] - Insertar nodo"<<endl;
        cout<<"[2] - Salir"<<endl;
        cout<<"Ingrese una opcion: ";
        cin>>opcion;

        if(opcion ==1){
            cout<<"\n Ingrese un valor para el nuevo nodo: ";
            cin>>dato;
            pila.Insertar(dato);
        }

    }while(opcion!=2);




return 0;
}
