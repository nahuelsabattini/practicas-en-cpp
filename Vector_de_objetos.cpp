#include <iostream>
#include <string.h>
#define N 2


using namespace std;


class Alumno{

private:
    string nombre;
    string apellido;
    int dni;
    float nota1;
    float nota2;

public:

    void setDatos(string nombre, string apellido, int dni, float nota1, float nota2){
        this->nombre = nombre;
        this->apellido = apellido;
        this->dni = dni;
        this->nota1 = nota1;
        this->nota2 = nota2;
    }

    string getDatos(){
        return "\nNombre: "+this->nombre+"\nApellido: "+this->apellido+"\nDNI: "+to_string(this->dni)+"\nNota 1:  "+to_string(this->nota1)+"\nNota 2:  "+to_string(this->nota2)+"\nPromedio: "+to_string((this->nota1+this->nota2)/2);

    }
};



int main(){

    Alumno alumnos[N] = Alumno();

    string nombre,apellido;
    int dni;
    float nota1,nota2;

    for(int i=0; i<N; i++){

        cout<<"\nIngrese el nombre del alumno "<<i+1<<": ";
        cin>>nombre;

        cout<<"\nIngrese el apellido del alumno "<<i+1<<": ";
        cin>>apellido;

        cout<<"\nIngrese el DNI del alumno "<<i+1<<": ";
        cin>>dni;

        cout<<"\nIngrese la nota 1 del alumno "<<i+1<<": ";
        cin>>nota1;

        cout<<"\nIngrese la nota 2 del alumno "<<i+1<<": ";
        cin>>nota2;

        alumnos[i].setDatos(nombre,apellido,dni,nota1,nota2);
        cout<<"\nAlumno registrado con exito"<<endl;
    }

    for(int j =0; j<N; j++){
        cout<<"\nAlumno ["<<j+1<<"]"<<endl;
        cout<<alumnos[j].getDatos()<<endl;
    }







return 0;
}
