#include <iostream>
#include <stdlib.h>

using namespace std;

struct nodo{

    int numero;
    nodo* siguiente;

}*primero, *ultimo;

void insertar_nodo(){

    nodo *nuevo_nodo = new nodo();

    cout<<"\nIngrese el valor para el nuevo nodo: ";
    cin>>nuevo_nodo->numero;

    if(primero==NULL){
        primero = nuevo_nodo;
        primero->siguiente = NULL;
        ultimo = primero;
    }else{
        ultimo->siguiente = nuevo_nodo;
        nuevo_nodo->siguiente = NULL;
        ultimo = nuevo_nodo;
    }
}

bool buscar_nodo(int valor_buscar){
    int contador=0;
    nodo *actual = new nodo();

    actual = primero;

    if(actual==NULL){
        cout<<"\nLista vacia.";
        return false;
    }else{

        while(actual!=NULL){
            contador++;
            if(actual->numero == valor_buscar){
                cout<<"Valor encontrado en la posicion: "<<contador<<endl;
                return true;
            }
            actual=actual->siguiente;
        }
        return false;
    }
}

bool modificar_dato_nodo(){

    int numero_modificado, numero_buscado;
    nodo *actual = new nodo();

    actual = primero;

    cout<<"\nIngrese el valor del nodo a buscar para modificar: ";
    cin>>numero_buscado;

    while(actual!=NULL){
            if(actual->numero == numero_buscado){
                cout<<"\nNODO ENCONTRADO"<<endl;
                cout<<"Ingrese el nuevo numero que desea colocar en este nodo: ";
                cin>>numero_modificado;
                actual->numero = numero_modificado;
                cout<<"Valor modificado con exito!"<<endl;
                return true;
            }
            actual = actual->siguiente;
    }
    cout<<"\nNodo no encontrado."<<endl;
}

int eliminar_nodo(){

    nodo *actual = new  nodo();
    nodo *anterior = new nodo();
    int valor_buscar;
    actual = primero;

    cout<<"Ingrese el valor del nodo que desea eliminar: ";
    cin>>valor_buscar;

    if(primero!=NULL){

            while(actual!=NULL){

                    if(actual->numero == valor_buscar){
                        if(actual==primero){
                            primero = primero->siguiente;
                        }else if(actual == ultimo){
                            anterior->siguiente = NULL;
                            ultimo = actual;
                        }else{
                            anterior->siguiente = actual->siguiente;
                        }
                        cout<<"\nNodo eliminado."<<endl;
                        return 0;
                    }
                    anterior=actual;
                    actual = actual->siguiente;
            }

    }else{
        cout<<"\nLista vacia."<<endl;
    }
}

int insertar_nodo_entre_2_nodos(){

    nodo *actual = new nodo();
    int valor_buscar;
    actual=primero;

    cout<<"�Al lado de que valor desea agregar su nuevo nodo?: ";
    cin>>valor_buscar;

    while(actual!=NULL){

            if(actual->numero == valor_buscar){
                cout<<"\nNUMERO ENCONTRADO... CREANDO NUEVO NODO"<<endl;
                nodo *nuevo_nodo_insertado = new nodo();
                cout<<"\nIngrese el numero del nuevo nodo insertado: ";
                cin>>nuevo_nodo_insertado->numero;
                nuevo_nodo_insertado->siguiente = actual->siguiente;
                actual->siguiente = nuevo_nodo_insertado;
                return 0;
            }

            actual = actual->siguiente;
   }

    cout<<"\nNODO NO ENCONTRADO."<<endl;
    return 0;

}

void insertar_nodo_al_final(){

    nodo *actual = new nodo();
    nodo *nuevo_nodo = new nodo();
    int valor_nodo_final;
    actual = ultimo;

    cout<<"\nIngrese un valor para el nuevo nodo: ";
    cin>>nuevo_nodo->numero;
    actual->siguiente = nuevo_nodo;
    nuevo_nodo->siguiente = NULL;
    ultimo = nuevo_nodo;

    cout<<"\nNodo creado al final de la lista con exito!"<<endl;
}

void mostrar_lista(){

    nodo *actual = new nodo();
    int contador =0;
    actual = primero;

    while(actual != NULL){
        cout<<"\nNodo "<<contador<<": "<<actual->numero<<endl;
        contador++;
        actual = actual->siguiente;
    }

}


int main(){

    int opcion, valor_buscar;

    while(1){

        cout<<"|-------------------------------------------------------------------|"<<endl;
        cout<<"|                        MENU CON LISTA SIMPLE                      |"<<endl;
        cout<<"|-------------------------------------------------------------------|"<<endl;
        cout<<"|Ingrese una opcion:                                                |"<<endl;
        cout<<"|-------------------------------------------------------------------|"<<endl;
        cout<<"| 1) Crear un nuevo nodo              |   2)Buscar nodo             |"<<endl;
        cout<<"| 3) Modoficar nodo                   |   4)Eliminar nodo           |"<<endl;
        cout<<"| 5) Insertar un nodo entre 2 nodos   |   6)Insertar nodo al final  |"<<endl;
        cout<<"| 7) Mostrar lista                    |   8)Salir                   |"<<endl;


        cout<<"\nIngrese una opcion: ";
        cin>>opcion;

        switch(opcion){
            case 1: insertar_nodo(); break; //listo

            case 2:
                cout<<"\nIngrese el valor que desea buscar: ";   //listo
                cin>>valor_buscar;

                if(buscar_nodo(valor_buscar) == false){
                    cout<<"Valor no encontrado.";
                }
                break;

            case 3: modificar_dato_nodo(); break; //listo
            case 4: eliminar_nodo(); break; //listo
            case 5: insertar_nodo_entre_2_nodos(); break;//listo
            case 6: insertar_nodo_al_final(); break; //listo
            case 7: mostrar_lista(); break; //listo
            case 8: exit(1); break;
            default: cout<<"Opcion incorrecta"<<endl;
        }
        system("pause");
        system("cls");
    }


return 0;
}
