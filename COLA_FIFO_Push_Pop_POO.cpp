/**
Programar la clase Cola (tipo de dato abstracto FIFO) mediante una lista enlazada
que contenga n�meros de DNIs de personas. Luego instancie una cola vac�a y
mediante un men� permita a�adir un elemento (push) y sacar un elemento (pop)
seg�n lo determine el usuario.
**/


#include <iostream>


using namespace std;

class Nodo{
public:
    int dni;
    Nodo *siguiente;
    Nodo(){this->dni=0; this->siguiente=NULL;}
};

class Cola{

public:

    Nodo *primero;
    Nodo *ultimo;

   // Cola(){this->primero=NULL; this->ultimo=NULL;}


    void push_elemento(){
        Nodo *nuevo_nodo = new Nodo();

        cout<<"\nIngrese el DNI: ";
        cin>>nuevo_nodo->dni;

        if(this->primero == NULL){

            this->primero=nuevo_nodo;
            primero->siguiente = NULL;
            this->ultimo = primero;
        }else{

            this->ultimo->siguiente = nuevo_nodo;
            nuevo_nodo->siguiente = NULL;
            this->ultimo = nuevo_nodo;

        }

    }


    void pop_elemento(){

        Nodo *actual = new Nodo();
        actual = this->primero;
        this->primero = actual->siguiente;
        cout<<"\nEl primer DNI ["<<actual->dni<<"] fue eliminado."<<endl;

    }


    void mostrar_lista(){
        Nodo *actual = new Nodo();
        actual = this->primero;
        while(actual!=NULL){
            cout<<"\nDNI: "<<actual->dni<<endl;
            actual = actual->siguiente;
        }

    }





};


int main(){

    Cola cola = Cola();
    int opcion;

    do{
        cout<<"\t\t\t\t\t|---------------------------------------|"<<endl;
        cout<<"\t\t\t\t\t| MENU CON LISTA SIMPLE: COLA (FIFO)    |"<<endl;
        cout<<"\t\t\t\t\t|---------------------------------------|"<<endl;
        cout<<"\t\t\t\t\t|      [1] - Insertar DNI (Push)        |"<<endl;
        cout<<"\t\t\t\t\t|      [2] - Eliminar DNI (Pop)         |"<<endl;
        cout<<"\t\t\t\t\t|      [3] - Mostrar lista              |"<<endl;
        cout<<"\t\t\t\t\t|      [4] - Salir                      |"<<endl;
        cout<<"\t\t\t\t\t ---------------------------------------"<<endl;

        cout<<"\nIngrese una opcion: ";
        cin>>opcion;

        switch(opcion){

            case 1: cola.push_elemento(); break;
            case 2: cola.pop_elemento(); break;
            case 3: cola.mostrar_lista(); break;
            case 4: exit(1); break;
            default: cout<<"\nOpcion incorrecta"<<endl;
        }

        cout<<"\n";
        system("pause");
        system("cls");

    }while(opcion != 3);

return 0;
}
