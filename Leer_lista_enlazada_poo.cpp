#include <iostream>

using namespace std;

class Nodo{

public:
    int dato;
    Nodo *siguiente;

    Nodo(){
        this->dato = 0;
        this->siguiente = NULL;
    }
};

class Pila{

public:
    int tamanio;
    Nodo *tope;

    Pila(){
        this->tamanio = 0;
        this->tope = NULL;
    }

    void insertar(int dato){
        Nodo *nuevo_nodo = new Nodo();

        nuevo_nodo->dato = dato;
        nuevo_nodo->siguiente=this->tope;
        this->tope = nuevo_nodo;
        cout<<"\nNodo agregado"<<endl;
        this->tamanio++;
    }

    void leer_pila(){
        Nodo *actual = new Nodo();
        actual = this->tope;

        while(actual!= NULL){
            cout<<"\n Valor: "<<actual->dato<<endl;
            actual = actual->siguiente;
        }
    }
};


int main(){

    int opcion, dato;

    Pila pila = Pila();

    do{
        cout<<"\t\t\t\t\t ----------------------------"<<endl;
        cout<<"\t\t\t\t\t|   [1] - Insertar nodo      |"<<endl;
        cout<<"\t\t\t\t\t|   [2] - Leer lista         |"<<endl;
        cout<<"\t\t\t\t\t|   [3] - Salir              |"<<endl;
        cout<<"\t\t\t\t\t ----------------------------"<<endl;

        cout<<"\nIngrese una opcion: ";
        cin>>opcion;

        if(opcion==1){

            cout<<"\nIngrese el  valor del nuevo nodo: ";
            cin>>dato;
            pila.insertar(dato);
        }else if(opcion==2){
            pila.leer_pila();
        }

        system("pause");
        system("cls");




    }while(opcion!=3);


return 0;
}







