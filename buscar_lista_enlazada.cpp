//Realizar un programa en c++ que busque un valor introducido en una lista enlazada e imprima todos los valores almacenados.

#include <iostream>

using namespace std;


struct nodo{
    int numero;
    nodo *siguiente;
}*primero, *ultimo;

void cargar_lista(){
    nodo *nuevo_valor = new nodo();

    cout<<"\nIngrese un valor: ";
    cin>>nuevo_valor->numero;

    if(primero == NULL){
        primero = nuevo_valor;
        primero->siguiente = NULL;
        ultimo = primero;
    }else{
        ultimo->siguiente = nuevo_valor;
        nuevo_valor->siguiente = NULL;
        ultimo = nuevo_valor;
    }

}

void leer_lista_enlazada(){
    nodo *actual = new nodo();

    actual = primero;

    cout<<"\n IMPRIMIENDO VALORES ALMACENADOS \n";

    if(actual == NULL){
        cout<<"Lista vacia.";
    }else{
        while(actual != NULL){
            cout<<"\nValor: "<<actual->numero;
            actual = actual->siguiente;
        }
    }
    cout<<"\n\n";
}

bool buscar_valor(int valor, int cantidad_datos){

    nodo *actual = new nodo();


    actual = primero;

    for(int i=0; i<cantidad_datos; i++){

        if(actual->numero == valor){
            cout<<"VALOR ENCONTRADO EN LA POSICION: "<<i+1;
            return true;
        }
        actual = actual->siguiente;
    }

    return false;
}


int main(){

    int cantidad_datos, valor;


    cout<<"Ingrese la cantidad de valores a registrar: ";
    cin>>cantidad_datos;

    for(int i=0; i<cantidad_datos; i++){
        cargar_lista();
    }

    cout<<"\nIngrese el valor que desea buscar: ";
    cin>>valor;

    if (buscar_valor(valor, cantidad_datos) == false){
            cout<<"Valor no encontrado";
    }

    leer_lista_enlazada();

return 0;
}
