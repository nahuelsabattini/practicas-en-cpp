#include <iostream>

using namespace std;

struct nodo{
    char nombre[30];
    nodo* siguiente;

}*primero, *ultimo; //punteros de tipo nodo


void cargar_nombes_lista(){

    nodo *nuevo = new nodo(); //este nodo registrara todos los nombres nuevos que se ingresen
    cout<<"Ingrese el nombre a registrar: ";
    fflush(stdin);
    cin.getline(nuevo->nombre,30,'\n'); //getline para poder utilizar espacios

    if(primero == NULL){ //si el primer nodo es nulo, significa que la lista est� vacia y hay que llenarla.
        primero = nuevo; //el primero pasa a ser el nuevo nodo (liena 14)
        primero->siguiente = NULL; //el puntero a sigueinte del primer nodo apuntar� a NULL (que se puede entender como el final de la lista)
        ultimo = primero; //como es el primer nodo creado, el primero tambien es el ultimo.
    }else{ //esto sucede en un 2do ciclo de ejecucion de la funci�n, luego de crear un nodo. Como el primer nodo no es nulo, se ejcuta lo siguiente.
        ultimo->siguiente = nuevo; //el puntero  sigueite de nuestro ultimo nodo, es igual al nuevo nodo (liena 14) que se crea en este 2do ciclo.
        nuevo->siguiente = NULL; //el puntero a siguiente del nuevo nodo apunta a null (final de la lista -por ahora-).
        ultimo = nuevo; //el nuevo nodo pasa a ser el ultimo.
    }
}

void leer_lista_enlazada(){
    nodo *actual = new nodo(); //creamos un nodo que sirva como "cabezal" para leer la lista.

    actual = primero; //lo "posicionamos" en el primer nodo

    if(actual == NULL){
        cout<<"La lista esta vacia";
    }else{
        while(actual != NULL){ //un ciclo que se repite hasta que el cabezal llegue al ultimo nodo.
            cout<<"Nombre: "<<actual->nombre<<endl; //imprime cada nombre almacenado en cada nodo.
            actual = actual->siguiente; //"deplazar el cabezal", haciendo que el puntero sigueite a actual apuente al nodo que sigue.
        }
    }
}


int main(){

    int cantidad_nombres;

    cout<<"Ingrese la cantidad de nombres que desea registrar: ";
    cin>>cantidad_nombres;

    for(int i=0; i<cantidad_nombres; i++){
        cargar_nombes_lista();
    }

    leer_lista_enlazada();
return 0;
}






