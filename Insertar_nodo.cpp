#include <iostream>

using namespace std;

struct nodo{

    int numero;
    nodo* siguiente;

}*primero, *ultimo;

void crear_lista_enlazada(){

    nodo *nuevo_numero = new nodo();

    cout<<"Ingrese un numero: ";
    cin>>nuevo_numero->numero;

    if(primero == NULL){
        primero = nuevo_numero;
        primero->siguiente = NULL;
        ultimo = primero;
    }else{
        ultimo->siguiente = nuevo_numero;
        nuevo_numero->siguiente = NULL;
        ultimo = nuevo_numero;
    }

}

bool buscar_nodo(int valor_buscar, int cantidad){

   // int cantidad;
    nodo *actual = new nodo();
    actual = primero;

    for(int i=0; i<cantidad; i++){
        if(actual->numero == valor_buscar){
            cout<<"VALOR ENCONTRADO!"<<endl;
            cout<<"VALOR: "<<actual->numero<<endl;
            return true;
        }
        actual = actual->siguiente;
    }
    return false;
}


int insertar_nodo(int valor_b){

    int valor_buscar;
    bool encontrado = false;
 //   nodo *nuevo_nodo = new nodo();
    nodo *actual = new nodo();

    actual = primero;
    cout<<"�Al lado de que valor de nodo desea agregar su nuevo nodo?: ";
    cin>>valor_buscar;


    while(actual != NULL){
        if(actual->numero == valor_buscar){
            cout<<"\n NODO Y VALOR ENCONTRADOS!"<<endl;
            nodo *nuevo_nodo = new nodo();
            cout<<"Por favor ingrese el valor del nuevo nodo: ";
            cin>>nuevo_nodo->numero;
            nuevo_nodo->siguiente = actual->siguiente;
            actual->siguiente = nuevo_nodo;

            encontrado = true;
            return 0;
        }

        actual = actual->siguiente;
    }

    if(encontrado==false){
        cout<<"\nVALOR NO ENCONTRADO"<<endl;
    }else{ cout<<"Finalizado.\n";}
}


void mostar_lista(){

    nodo *actual = new nodo();
    actual = primero;

    if(actual == NULL){
        cout<<"Lista vacia"<<endl;
    }else{
        while(actual!=NULL){
            cout<<"VALOR: "<<actual->numero<<endl;
            actual = actual->siguiente;
        }
    }


}


int main(){

    int cantidad,valor_buscar;

    cout<<"Ingrese la cantidad de nodos que desea crear: ";
    cin>>cantidad;

    for(int i=0; i<cantidad; i++){crear_lista_enlazada();}

    cout<<"Lista enlazada creada"<<endl;

    cout<<"Ingrese el valor que desea buscar: ";
    cin>>valor_buscar;

    if(buscar_nodo(valor_buscar,cantidad)==false){cout<<"\nValor no encontado.";}

    insertar_nodo(valor_buscar);
    mostar_lista();


return 0;
}
