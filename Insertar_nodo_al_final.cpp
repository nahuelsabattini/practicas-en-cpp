#include <iostream>

using namespace std;


struct nodo{
    int documento;
    nodo* siguiente;


}*primero, *ultimo;



void crear_lista(){
    nodo *nuevo_nodo = new nodo();

    cout<<"Ingrese el numero de documento: ";
    cin>>nuevo_nodo->documento;

    if(primero == NULL){
        primero = nuevo_nodo;
        primero->siguiente = NULL;
        ultimo = primero;
    }else{
        ultimo->siguiente = nuevo_nodo;
        nuevo_nodo->siguiente = NULL;
        ultimo = nuevo_nodo;
    }

}


void insertar_nodo_al_final(){
    nodo *actual = new nodo();
    actual=ultimo;
    nodo *nuevo_nodo = new nodo();
    cout<<"Ingrese el documento del nuevo nodo (se agregara al final): ";
    cin>>nuevo_nodo->documento;
    actual->siguiente = nuevo_nodo;
    nuevo_nodo->siguiente = NULL;
    ultimo = nuevo_nodo;
}

void leer_lista(){
    nodo *actual = new nodo();

    actual=primero;

    while(actual!=NULL){
        cout<<"Valor: "<<actual->documento<<endl;
        actual = actual->siguiente;
    }


}


int main(){
    crear_lista();
    crear_lista();
    crear_lista();

    insertar_nodo_al_final();
    leer_lista();
    insertar_nodo_al_final();
    leer_lista();

return 0;
}
