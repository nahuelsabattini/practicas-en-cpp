#include <iostream>

using namespace std;


class Persona{

private:
    string nombre;
    string apellido;

public:

    void Insertar(string nombre, string apellido){
        this->nombre = nombre;
        this->apellido = apellido;
    }

    string imprimir(){
        return "Nombre: "+this->nombre+"Apellido: "+this->apellido;
    }


};

class Alumno : public Persona{

public:
    void imprimir_2(){

        cout<<this->imprimir()<<endl;
    }


};




int main(){

    //Persona alumno = Persona();
    Alumno nahuel = Alumno();

    nahuel.Insertar("nahuel","sabattini");
    nahuel.imprimir_2();

return 0;
}
